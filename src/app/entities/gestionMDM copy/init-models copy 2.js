var DataTypes = require("sequelize").DataTypes;
var _Canvas = require("./Canvas");
var _CanvasLigne = require("./CanvasLigne");
var _MDM_FORMAT_DONNEE = require("./FormatDonnee");
var _Model = require("./Model");
var _Motif = require("./Motif");
var _Param = require("./Param");
var _ParamLigneCanvas = require("./ParamCanvasLigne");

function initModels(sequelize) {
  var Canvas = _Canvas(sequelize, DataTypes);
  var CanvasLigne = _CanvasLigne(sequelize, DataTypes);
  var MDM_FORMAT_DONNEE = _MDM_FORMAT_DONNEE(sequelize, DataTypes);
  var Model = _Model(sequelize, DataTypes);
  var Motif = _Motif(sequelize, DataTypes);
  var Param = _Param(sequelize, DataTypes);
  var ParamLigneCanvas = _ParamLigneCanvas(sequelize, DataTypes);

  CanvasLigne.belongsTo(Canvas, {
    as: "CANVAS",
    foreignKey: "ID_CANVAS",
  });
  Canvas.hasMany(CanvasLigne, {
    as: "CANVAS_LIGNES",
    foreignKey: "ID_CANVAS",
  });
  Param.belongsTo(Canvas, { as: "CANVAS", foreignKey: "ID_CANVAS" });
  Canvas.hasMany(Param, { as: "PARAMS", foreignKey: "ID_CANVAS" });
  ParamLigneCanvas.belongsTo(CanvasLigne, {
    as: "CANVAS_LIGNE",
    foreignKey: "ID_CANVAS_LIGNE",
  });
  CanvasLigne.hasMany(ParamLigneCanvas, {
    as: "PARAM_LIGNE_CANVASS",
    foreignKey: "ID_CANVAS_LIGNE",
  });
  Param.belongsTo(MDM_FORMAT_DONNEE, {
    as: "FORMAT_DONNEE",
    foreignKey: "ID_FORMAT_DONNEE",
  });
  MDM_FORMAT_DONNEE.hasMany(Param, {
    as: "PARAMS",
    foreignKey: "ID_FORMAT_DONNEE",
  });
  Canvas.belongsTo(Model, { as: "MODEL", foreignKey: "ID_Model" });
  Model.hasMany(Canvas, { as: "CANVASS", foreignKey: "ID_Model" });
  ParamLigneCanvas.belongsTo(Param, {
    as: "PARAM",
    foreignKey: "ID_PARAM",
  });
  Param.hasMany(ParamLigneCanvas, {
    as: "PARAM_LIGNE_CANVASS",
    foreignKey: "ID_PARAM",
  });
  Motif.belongsTo(ParamLigneCanvas, {
    as: "PARAM_LIGNE_CANVAS",
    foreignKey: "ID_PARAM_CONTENU",
  });
  ParamLigneCanvas.hasMany(Motif, {
    as: "MOTIFS",
    foreignKey: "ID_PARAM_CONTENU",
  });

  return {
    Canvas,
    CanvasLigne,
    MDM_FORMAT_DONNEE,
    Model,
    Motif,
    Param,
    ParamLigneCanvas,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
