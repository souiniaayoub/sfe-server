const Sequelize = require("sequelize");
module.exports = function (sequelize, DataTypes) {
  return sequelize.define(
    "Canvas", {
      ID_CANVAS: {
        autoIncrement: true,
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
      },
      LIBELLE: {
        type: DataTypes.STRING(50),
        allowNull: false,
        unique: "UQ__MDM_CANV__64586F3ABAAFFD4F",
      },
      ACTIF: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      ID_MDM_MODEL: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: "Model",
          key: "ID_MDM_MODEL",
        },
      },
      ID_OP_SAISIE: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: "A_UTILISATEUR",
          key: "ID_UTILISATEUR",
        },
      },
      DATETIME_SAISIE: {
        type: DataTypes.DATE,
        allowNull: false,
      },
    }, {
      sequelize,
      tableName: "MDM_CANVAS",
      schema: "dbo",
      timestamps: false,
      indexes: [{
          name: "PK_MDM_CANVAS",
          unique: true,
          fields: [{
            name: "ID_CANVAS"
          }],
        },
        {
          name: "UQ__MDM_CANV__64586F3ABAAFFD4F",
          unique: true,
          fields: [{
            name: "LIBELLE"
          }],
        },
      ],
    }
  );
};