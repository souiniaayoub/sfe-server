const Sequelize = require("sequelize");
module.exports = function (sequelize, DataTypes) {
  return sequelize.define(
    "ParamCanvasLigne",
    {
      ID_PARAM_LIGNE_CANVAS: {
        autoIncrement: true,
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
      },
      ID_CANVAS_LIGNE: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: "CanvasLigne",
          key: "ID_CANVAS_LIGNE",
        },
      },
      ID_PARAM: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: "Param",
          key: "ID_PARAM",
        },
      },
      CONTENU: {
        type: DataTypes.STRING(50),
        allowNull: true,
      },
      VALID: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      ID_OP_SAISIE: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: "A_UTILISATEUR",
          key: "ID_UTILISATEUR",
        },
      },
      DATETIME_SAISIE: {
        type: DataTypes.DATE,
        allowNull: false,
      },
    },
    {
      sequelize,
      tableName: "MDM_PARAM_LIGNE_CANVAS",
      schema: "dbo",
      timestamps: false,
      indexes: [
        {
          name: "PK_MDM_PARAM_LIGNE_CANVAS",
          unique: true,
          fields: [{ name: "ID_PARAM_LIGNE_CANVAS" }],
        },
      ],
    }
  );
};
