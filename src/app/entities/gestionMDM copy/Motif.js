const Sequelize = require("sequelize");
module.exports = function (sequelize, DataTypes) {
  return sequelize.define(
    "Motif",
    {
      ID_MOTIF: {
        autoIncrement: true,
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
      },
      ID_PARAM_CONTENU: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: "ParamCanvasLigne",
          key: "ID_PARAM_LIGNE_CANVAS",
        },
      },
      MOTIF: {
        type: DataTypes.STRING(150),
        allowNull: true,
      },
      VERIFIER: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      ID_OP_VERIFIER: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: "A_UTILISATEUR",
          key: "ID_UTILISATEUR",
        },
      },
      DATETIME_VERIFIER: {
        type: DataTypes.DATE,
        allowNull: false,
      },
    },
    {
      sequelize,
      tableName: "MDM_MOTIF",
      schema: "dbo",
      timestamps: false,
      indexes: [
        {
          name: "PK_MDM_MOTIF",
          unique: true,
          fields: [{ name: "ID_MOTIF" }],
        },
      ],
    }
  );
};
