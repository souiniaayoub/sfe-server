var DataTypes = require("sequelize").DataTypes;
var _MDM_CANVAS = require("./Canvas");
var _MDM_CANVAS_LIGNE = require("./CanvasLigne");
var _MDM_FORMAT_DONNEE = require("./FormatDonnee");
var _MDM_MODEL = require("./Model");
var _MDM_MOTIF = require("./Motif");
var _MDM_PARAM = require("./Param");
var _MDM_PARAM_LIGNE_CANVAS = require("./ParamCanvasLigne");

function initModels(sequelize) {
  var MDM_CANVAS = _MDM_CANVAS(sequelize, DataTypes);
  var MDM_CANVAS_LIGNE = _MDM_CANVAS_LIGNE(sequelize, DataTypes);
  var MDM_FORMAT_DONNEE = _MDM_FORMAT_DONNEE(sequelize, DataTypes);
  var MDM_MODEL = _MDM_MODEL(sequelize, DataTypes);
  var MDM_MOTIF = _MDM_MOTIF(sequelize, DataTypes);
  var MDM_PARAM = _MDM_PARAM(sequelize, DataTypes);
  var MDM_PARAM_LIGNE_CANVAS = _MDM_PARAM_LIGNE_CANVAS(sequelize, DataTypes);

  MDM_CANVAS_LIGNE.belongsTo(MDM_CANVAS, {
    as: "CANVAS",
    foreignKey: "ID_CANVAS",
  });
  MDM_CANVAS.hasMany(MDM_CANVAS_LIGNE, {
    as: "CANVAS_LIGNES",
    foreignKey: "ID_CANVAS",
  });
  MDM_PARAM.belongsTo(MDM_CANVAS, { as: "CANVAS", foreignKey: "ID_CANVAS" });
  MDM_CANVAS.hasMany(MDM_PARAM, { as: "PARAMS", foreignKey: "ID_CANVAS" });
  MDM_PARAM_LIGNE_CANVAS.belongsTo(MDM_CANVAS_LIGNE, {
    as: "CANVAS_LIGNE",
    foreignKey: "ID_CANVAS_LIGNE",
  });
  MDM_CANVAS_LIGNE.hasMany(MDM_PARAM_LIGNE_CANVAS, {
    as: "PARAM_LIGNE_CANVASS",
    foreignKey: "ID_CANVAS_LIGNE",
  });
  MDM_PARAM.belongsTo(MDM_FORMAT_DONNEE, {
    as: "FORMAT_DONNEE",
    foreignKey: "ID_FORMAT_DONNEE",
  });
  MDM_FORMAT_DONNEE.hasMany(MDM_PARAM, {
    as: "PARAMS",
    foreignKey: "ID_FORMAT_DONNEE",
  });
  MDM_CANVAS.belongsTo(MDM_MODEL, { as: "MODEL", foreignKey: "ID_MDM_MODEL" });
  MDM_MODEL.hasMany(MDM_CANVAS, { as: "CANVASS", foreignKey: "ID_MDM_MODEL" });
  MDM_PARAM_LIGNE_CANVAS.belongsTo(MDM_PARAM, {
    as: "PARAM",
    foreignKey: "ID_PARAM",
  });
  MDM_PARAM.hasMany(MDM_PARAM_LIGNE_CANVAS, {
    as: "PARAM_LIGNE_CANVASS",
    foreignKey: "ID_PARAM",
  });
  MDM_MOTIF.belongsTo(MDM_PARAM_LIGNE_CANVAS, {
    as: "PARAM_LIGNE_CANVAS",
    foreignKey: "ID_PARAM_CONTENU",
  });
  MDM_PARAM_LIGNE_CANVAS.hasMany(MDM_MOTIF, {
    as: "MOTIFS",
    foreignKey: "ID_PARAM_CONTENU",
  });

  return {
    MDM_CANVAS,
    MDM_CANVAS_LIGNE,
    MDM_FORMAT_DONNEE,
    MDM_MODEL,
    MDM_MOTIF,
    MDM_PARAM,
    MDM_PARAM_LIGNE_CANVAS,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
