const dbintegrale = require("../../../../env/entitiy_connection")(
  "dbintegrale"
);
const initmodels = require("./init-models").initModels;

module.exports = { ...initmodels(dbintegrale), db: dbintegrale };
