const Sequelize = require("sequelize");
module.exports = function (sequelize, DataTypes) {
  return sequelize.define(
    "Model", {
      ID_MDM_MODEL: {
        autoIncrement: true,
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
      },
      LIBELLE: {
        type: DataTypes.STRING(50),
        allowNull: false,
        unique: "UQ__MDM_MODE__64586F3A51C3FE58",
      },
      ACTIF: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      ID_OP_SAISIE: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: "A_UTILISATEUR",
          key: "ID_UTILISATEUR",
        },
      },
      DATETIME_SAISIE: {
        type: DataTypes.DATE,
        allowNull: false,
      },
    }, {
      sequelize,
      tableName: "MDM_MODEL",
      schema: "dbo",
      timestamps: false,
      indexes: [{
          name: "PK_MDM_MODEL",
          unique: true,
          fields: [{
            name: "ID_MDM_MODEL"
          }],
        },
        {
          name: "UQ__MDM_MODE__64586F3A51C3FE58",
          unique: true,
          fields: [{
            name: "LIBELLE"
          }],
        },
      ],
    }
  );
};