const Sequelize = require("sequelize");
module.exports = function (sequelize, DataTypes) {
  return sequelize.define(
    "FormatDonnee",
    {
      ID_FORMAT_DONNEE: {
        autoIncrement: true,
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
      },
      LIBELLE_FORMAT_DONNEE: {
        type: DataTypes.STRING(50),
        allowNull: false,
        unique: "UQ__MDM_FORM__132D129A8595352C",
      },
      ID_OP_SAISIE: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: "A_UTILISATEUR",
          key: "ID_UTILISATEUR",
        },
      },
      DATETIME_SAISIE: {
        type: DataTypes.DATE,
        allowNull: false,
      },
    },
    {
      sequelize,
      tableName: "MDM_FORMAT_DONNEE",
      schema: "dbo",
      timestamps: false,
      indexes: [
        {
          name: "PK_MDM_FORMAT_DONNEE",
          unique: true,
          fields: [{ name: "ID_FORMAT_DONNEE" }],
        },
        {
          name: "UQ__MDM_FORM__132D129A8595352C",
          unique: true,
          fields: [{ name: "LIBELLE_FORMAT_DONNEE" }],
        },
      ],
    }
  );
};
