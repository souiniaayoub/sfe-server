var DataTypes = require("sequelize").DataTypes;
var _Canvas = require("./Canvas");
var _CanvasLigne = require("./CanvasLigne");
var _FormatDonnee = require("./FormatDonnee");
var _Model = require("./Model");
var _Motif = require("./Motif");
var _Param = require("./Param");
var _ParamLigneCanvas = require("./ParamCanvasLigne");

function initModels(sequelize) {
  var Canvas = _Canvas(sequelize, DataTypes);
  var CanvasLigne = _CanvasLigne(sequelize, DataTypes);
  var FormatDonnee = _FormatDonnee(sequelize, DataTypes);
  var Model = _Model(sequelize, DataTypes);
  var Motif = _Motif(sequelize, DataTypes);
  var Param = _Param(sequelize, DataTypes);
  var ParamLigneCanvas = _ParamLigneCanvas(sequelize, DataTypes);

  CanvasLigne.belongsTo(Canvas, {
    as: "CANVAS",
    foreignKey: "ID_CANVAS",
  });
  Canvas.hasMany(CanvasLigne, {
    as: "CANVAS_LIGNES",
    foreignKey: "ID_CANVAS",
  });
  Param.belongsTo(Canvas, { as: "CANVAS", foreignKey: "ID_CANVAS" });
  Canvas.hasMany(Param, { as: "PARAMS", foreignKey: "ID_CANVAS" });
  ParamLigneCanvas.belongsTo(CanvasLigne, {
    as: "CANVAS_LIGNE",
    foreignKey: "ID_CANVAS_LIGNE",
  });
  CanvasLigne.hasMany(ParamLigneCanvas, {
    as: "PARAM_LIGNE_CANVAS",
    foreignKey: "ID_CANVAS_LIGNE",
  });
  Param.belongsTo(FormatDonnee, {
    as: "FORMAT_DONNEE",
    foreignKey: "ID_FORMAT_DONNEE",
  });
  FormatDonnee.hasMany(Param, {
    as: "PARAMS",
    foreignKey: "ID_FORMAT_DONNEE",
  });
  Canvas.belongsTo(Model, { as: "MODEL", foreignKey: "ID_MDM_MODEL" });
  Model.hasMany(Canvas, { as: "CANVAS", foreignKey: "ID_MDM_MODEL" });
  ParamLigneCanvas.belongsTo(Param, {
    as: "PARAM",
    foreignKey: "ID_PARAM",
  });
  Param.hasMany(ParamLigneCanvas, {
    as: "PARAM_LIGNE_CANVAS",
    foreignKey: "ID_PARAM",
  });
  Motif.belongsTo(ParamLigneCanvas, {
    as: "PARAM_LIGNE_CANVAS",
    foreignKey: "ID_PARAM_CONTENU",
    // foreignKey: "ID_PARAM_CONTENU",
    // sourceKey: "ID_PARAM_LIGNE_CANVAS",
  });
  ParamLigneCanvas.hasMany(Motif, {
    as: "MOTIFS",
    foreignKey: "ID_PARAM_CONTENU",
    // foreignKey: "ID_PARAM_CONTENU",
    // sourceKey: "ID_PARAM_LIGNE_CANVAS",
  });

  return {
    Canvas,
    CanvasLigne,
    FormatDonnee,
    Model,
    Motif,
    Param,
    ParamLigneCanvas,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
