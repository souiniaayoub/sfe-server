const Sequelize = require("sequelize");
module.exports = function (sequelize, DataTypes) {
  return sequelize.define(
    "CanvasLigne",
    {
      ID_CANVAS_LIGNE: {
        autoIncrement: true,
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
      },
      ID_CANVAS: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: "Canvas",
          key: "ID_CANVAS",
        },
      },
      SYNCHRO: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      ANNULER: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      ID_OP_SAISIE: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: "A_UTILISATEUR",
          key: "ID_UTILISATEUR",
        },
      },
      DATETIME_SAISIE: {
        type: DataTypes.DATE,
        allowNull: false,
      },
    },
    {
      sequelize,
      tableName: "MDM_CANVAS_LIGNE",
      schema: "dbo",
      timestamps: false,
      indexes: [
        {
          name: "PK_MDM_CANVAS_LIGNE",
          unique: true,
          fields: [{ name: "ID_CANVAS_LIGNE" }],
        },
      ],
    }
  );
};
