const Sequelize = require("sequelize");
module.exports = function (sequelize, DataTypes) {
  return sequelize.define(
    "Param", {
      ID_PARAM: {
        autoIncrement: true,
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
      },
      LIBELLE_PARAM: {
        type: DataTypes.STRING(50),
        allowNull: false,
      },
      QUERY_DONNE_BASE: {
        type: DataTypes.STRING(150),
        allowNull: true,
      },
      VALEUR_AFFICHER: {
        type: DataTypes.STRING(50),
        allowNull: true,
      },
      VALEUR_RETOUR: {
        type: DataTypes.STRING(50),
        allowNull: true,
      },
      ID_FORMAT_DONNEE: {
        type: DataTypes.INTEGER,
        allowNull: true,
        references: {
          model: "FormatDonnee",
          key: "ID_FORMAT_DONNEE",
        },
      },
      ID_CANVAS: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: "Canvas",
          key: "ID_CANVAS",
        },
      },
      ID_OP_SAISIE: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: "A_UTILISATEUR",
          key: "ID_UTILISATEUR",
        },
      },
      DATETIME_SAISIE: {
        type: DataTypes.DATE,
        allowNull: false,
      },
      IS_PRIMARY_KEY: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
    }, {
      sequelize,
      tableName: "MDM_PARAM",
      schema: "dbo",
      timestamps: false,
      indexes: [{
        name: "PK_MDM_PARAM",
        unique: true,
        fields: [{
          name: "ID_PARAM"
        }],
      }, ],
    }
  );
};