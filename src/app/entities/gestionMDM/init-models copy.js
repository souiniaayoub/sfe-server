var DataTypes = require("sequelize").DataTypes;
var _MDM_CANVAS = require("./Canvas");
var _MDM_CANVAS_LIGNE = require("./CanvasLigne");
var _MDM_FORMAT_DONNEE = require("./FormatDonnee");
var _MDM_MODEL = require("./Model");
var _MDM_MOTIF = require("./Motif");
var _MDM_PARAM = require("./Param");
var _MDM_PARAM_LIGNE_CANVAS = require("./ParamCanvasLigne");

function initModels(sequelize) {
  var MDM_CANVAS = _MDM_CANVAS(sequelize, DataTypes);
  var MDM_CANVAS_LIGNE = _MDM_CANVAS_LIGNE(sequelize, DataTypes);
  var MDM_FORMAT_DONNEE = _MDM_FORMAT_DONNEE(sequelize, DataTypes);
  var MDM_MODEL = _MDM_MODEL(sequelize, DataTypes);
  var MDM_MOTIF = _MDM_MOTIF(sequelize, DataTypes);
  var MDM_PARAM = _MDM_PARAM(sequelize, DataTypes);
  var MDM_PARAM_LIGNE_CANVAS = _MDM_PARAM_LIGNE_CANVAS(sequelize, DataTypes);

  MDM_CANVAS.belongsTo(A_UTILISATEUR, { as: "ID_OP_SAISIE_A_UTILISATEUR", foreignKey: "ID_OP_SAISIE"});
  A_UTILISATEUR.hasMany(MDM_CANVAS, { as: "MDM_CANVASes", foreignKey: "ID_OP_SAISIE"});
  MDM_CANVAS_LIGNE.belongsTo(A_UTILISATEUR, { as: "ID_OP_SAISIE_A_UTILISATEUR", foreignKey: "ID_OP_SAISIE"});
  A_UTILISATEUR.hasMany(MDM_CANVAS_LIGNE, { as: "MDM_CANVAS_LIGNEs", foreignKey: "ID_OP_SAISIE"});
  MDM_FORMAT_DONNEE.belongsTo(A_UTILISATEUR, { as: "ID_OP_SAISIE_A_UTILISATEUR", foreignKey: "ID_OP_SAISIE"});
  A_UTILISATEUR.hasMany(MDM_FORMAT_DONNEE, { as: "MDM_FORMAT_DONNEEs", foreignKey: "ID_OP_SAISIE"});
  MDM_MODEL.belongsTo(A_UTILISATEUR, { as: "ID_OP_SAISIE_A_UTILISATEUR", foreignKey: "ID_OP_SAISIE"});
  A_UTILISATEUR.hasMany(MDM_MODEL, { as: "MDM_MODELs", foreignKey: "ID_OP_SAISIE"});
  MDM_MOTIF.belongsTo(A_UTILISATEUR, { as: "ID_OP_VERIFIER_A_UTILISATEUR", foreignKey: "ID_OP_VERIFIER"});
  A_UTILISATEUR.hasMany(MDM_MOTIF, { as: "MDM_MOTIFs", foreignKey: "ID_OP_VERIFIER"});
  MDM_PARAM.belongsTo(A_UTILISATEUR, { as: "ID_OP_SAISIE_A_UTILISATEUR", foreignKey: "ID_OP_SAISIE"});
  A_UTILISATEUR.hasMany(MDM_PARAM, { as: "MDM_PARAMs", foreignKey: "ID_OP_SAISIE"});
  MDM_PARAM_LIGNE_CANVAS.belongsTo(A_UTILISATEUR, { as: "ID_OP_SAISIE_A_UTILISATEUR", foreignKey: "ID_OP_SAISIE"});
  A_UTILISATEUR.hasMany(MDM_PARAM_LIGNE_CANVAS, { as: "MDM_PARAM_LIGNE_CANVASes", foreignKey: "ID_OP_SAISIE"});
  MDM_CANVAS_LIGNE.belongsTo(MDM_CANVAS, { as: "ID_CANVAS_MDM_CANVAS", foreignKey: "ID_CANVAS"});
  MDM_CANVAS.hasMany(MDM_CANVAS_LIGNE, { as: "MDM_CANVAS_LIGNEs", foreignKey: "ID_CANVAS"});
  MDM_PARAM.belongsTo(MDM_CANVAS, { as: "ID_CANVAS_MDM_CANVAS", foreignKey: "ID_CANVAS"});
  MDM_CANVAS.hasMany(MDM_PARAM, { as: "MDM_PARAMs", foreignKey: "ID_CANVAS"});
  MDM_PARAM_LIGNE_CANVAS.belongsTo(MDM_CANVAS_LIGNE, { as: "ID_CANVAS_LIGNE_MDM_CANVAS_LIGNE", foreignKey: "ID_CANVAS_LIGNE"});
  MDM_CANVAS_LIGNE.hasMany(MDM_PARAM_LIGNE_CANVAS, { as: "MDM_PARAM_LIGNE_CANVASes", foreignKey: "ID_CANVAS_LIGNE"});
  MDM_PARAM.belongsTo(MDM_FORMAT_DONNEE, { as: "ID_FORMAT_DONNEE_MDM_FORMAT_DONNEE", foreignKey: "ID_FORMAT_DONNEE"});
  MDM_FORMAT_DONNEE.hasMany(MDM_PARAM, { as: "MDM_PARAMs", foreignKey: "ID_FORMAT_DONNEE"});
  MDM_CANVAS.belongsTo(MDM_MODEL, { as: "ID_MDM_MODEL_MDM_MODEL", foreignKey: "ID_MDM_MODEL"});
  MDM_MODEL.hasMany(MDM_CANVAS, { as: "MDM_CANVASes", foreignKey: "ID_MDM_MODEL"});
  MDM_PARAM_LIGNE_CANVAS.belongsTo(MDM_PARAM, { as: "ID_PARAM_MDM_PARAM", foreignKey: "ID_PARAM"});
  MDM_PARAM.hasMany(MDM_PARAM_LIGNE_CANVAS, { as: "MDM_PARAM_LIGNE_CANVASes", foreignKey: "ID_PARAM"});
  MDM_MOTIF.belongsTo(MDM_PARAM_LIGNE_CANVAS, { as: "ID_PARAM_CONTENU_MDM_PARAM_LIGNE_CANVAS", foreignKey: "ID_PARAM_CONTENU"});
  MDM_PARAM_LIGNE_CANVAS.hasMany(MDM_MOTIF, { as: "MDM_MOTIFs", foreignKey: "ID_PARAM_CONTENU"});

  return {
    MDM_CANVAS,
    MDM_CANVAS_LIGNE,
    MDM_FORMAT_DONNEE,
    MDM_MODEL,
    MDM_MOTIF,
    MDM_PARAM,
    MDM_PARAM_LIGNE_CANVAS,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
