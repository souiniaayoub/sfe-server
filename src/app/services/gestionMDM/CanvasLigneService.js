const {
  CanvasLigne,
  ParamLigneCanvas,
  db,
  Param,
  Canvas
} = require("../../entities/gestionMDM");
const {
  WsResponse
} = require("../../helpers/ws-response/ws.response");
const {
  HttpStatus
} = require("../../helpers/http-status/http-status.enum");

module.exports = {
  create: (CanvasLigneObjet) => {
    return CanvasLigne.create(CanvasLigneObjet)
      .then((data) => {
        return new WsResponse(HttpStatus.OK, "Opération succés", data);
      })
      .catch((err) => {
        throw new WsResponse(
          HttpStatus.INTERNAL_SERVER_ERROR,
          err.message,
          err
        );
      });
  },
  findAllIncludeParamLigneCanvas: (ID_CANVAS) => {
    return CanvasLigne.findAll({
        include: [{
          model: ParamLigneCanvas,
          as: "PARAM_LIGNE_CANVAS",
          include: [{
            model: Param,
            as: "PARAM",
          }],
        }],
        where: {
          ID_CANVAS: ID_CANVAS
        }
      })
      .then((data) => {
        return new WsResponse(
          HttpStatus.OK,
          `Line count: ${data.length + 1}`,
          data
        );
      })
      .catch((err) => {
        throw new WsResponse(
          HttpStatus.INTERNAL_SERVER_ERROR,
          err.message,
          err
        );
      });
  },
  // findAllByParamPK: (Value) => {
  //   return CanvasLigne.findAll({
  //       include: [{
  //         model: ParamLigneCanvas,
  //         as: "PARAM_LIGNE_CANVAS",
  //         where: {
  //           CONTENU: Value
  //         },
  //         include: [{
  //           model: Param,
  //           as: "PARAM",
  //           where: {
  //             IS_PRIMARY_KEY: true
  //           },
  //         }],
  //       }],
  //     })
  //     .then((data) => {
  //       return new WsResponse(
  //         HttpStatus.OK,
  //         `Line count: ${data.length + 1}`,
  //         data
  //       );
  //     })
  //     .catch((err) => {
  //       throw new WsResponse(
  //         HttpStatus.INTERNAL_SERVER_ERROR,
  //         err.message,
  //         err
  //       );
  //     });
  // },


  findAllByParamPK: (Value) => {
    return ParamLigneCanvas.findAll({
        include: [{
          model: CanvasLigne,
          as: "CANVAS_LIGNE",

          include: [{
            model: ParamLigneCanvas,
            as: "PARAM_LIGNE_CANVAS",
            include: [{
              model: Param,
              as: "PARAM",
            }],
          }, {
            model: Canvas,
            as: "CANVAS",
            include: [{
              model: Param,
              as: "PARAMS",
            }]
          }],
        }, {
          model: Param,
          as: "PARAM",
          where: {
            IS_PRIMARY_KEY: true
          },
        }],
        where: {
          CONTENU: Value
        }
      })
      .then((data) => {
        return new WsResponse(
          HttpStatus.OK,
          `Line count: ${data.length + 1}`,
          data
        );
      })
      .catch((err) => {
        throw new WsResponse(
          HttpStatus.INTERNAL_SERVER_ERROR,
          err.message,
          err
        );
      });
  },
  NonValidParams: (ID_CANVAS_LIGNE) => {
    return CanvasLigne.findAll({
        include: [{
          model: ParamLigneCanvas,
          as: "PARAM_LIGNE_CANVAS",
          where: {
            VALID: false
          }
        }],
        where: {
          ID_CANVAS_LIGNE: ID_CANVAS_LIGNE
        }
      })
      .then((data) => {
        return new WsResponse(
          HttpStatus.OK,
          `Line count: ${data.length + 1}`,
          data
        );
      })
      .catch((err) => {
        throw new WsResponse(
          HttpStatus.INTERNAL_SERVER_ERROR,
          err.message,
          err
        );
      });
  },
  createWithParams: (CanvasLigneObjet) => {
    return CanvasLigne.create(CanvasLigneObjet, {
        include: [{
          model: ParamLigneCanvas,
          as: "PARAM_LIGNE_CANVAS",
        }, ],
      })
      .then((data) => {
        return new WsResponse(HttpStatus.OK, "Opération succés", data);
      })
      .catch((err) => {
        throw new WsResponse(
          HttpStatus.INTERNAL_SERVER_ERROR,
          err.message,
          err
        );
      });
  },
  update: (CanvasLigneObjet) => {
    return CanvasLigne.update(CanvasLigneObjet, {
        where: {
          ID_CANVAS_LIGNE: CanvasLigneObjet.ID_CANVAS_LIGNE
        },
      })
      .then((data) => {
        return new WsResponse(HttpStatus.OK, "Opération succés", data);
      })
      .catch((err) => {
        throw new WsResponse(
          HttpStatus.INTERNAL_SERVER_ERROR,
          err.message,
          err
        );
      });
  },
};