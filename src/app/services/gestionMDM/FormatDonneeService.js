const { FormatDonnee, db } = require("../../entities/gestionMDM");
const { WsResponse } = require("../../helpers/ws-response/ws.response");
const { HttpStatus } = require("../../helpers/http-status/http-status.enum");

module.exports = {
  create: (FormatDonneeObjet) => {
    return FormatDonnee.create(FormatDonneeObjet)
      .then((data) => {
        return new WsResponse(HttpStatus.OK, "Opération succés", data);
      })
      .catch((err) => {
        throw new WsResponse(
          HttpStatus.INTERNAL_SERVER_ERROR,
          err.message,
          err
        );
      });
  },
  findAll: () => {
    return FormatDonnee.findAll()
      .then((data) => {
        return new WsResponse(
          HttpStatus.OK,
          `Line count: ${data.length + 1}`,
          data
        );
      })
      .catch((err) => {
        throw new WsResponse(
          HttpStatus.INTERNAL_SERVER_ERROR,
          err.message,
          err
        );
      });
  },
};
