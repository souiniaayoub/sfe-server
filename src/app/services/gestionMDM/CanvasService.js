const {
  Canvas,
  Param,
  Model,
  FormatDonnee,
  db,
  CanvasParam,
} = require("../../entities/gestionMDM");
const {
  WsResponse
} = require("../../helpers/ws-response/ws.response");
const {
  HttpStatus
} = require("../../helpers/http-status/http-status.enum");

module.exports = {
  create: (CanvasObjet) => {
    return Canvas.create(CanvasObjet)
      .then((data) => {
        return new WsResponse(HttpStatus.OK, "Opération succés", data);
      })
      .catch((err) => {
        throw new WsResponse(
          HttpStatus.INTERNAL_SERVER_ERROR,
          err.message,
          err
        );
      });
  },
  findAll: () => {
    return Canvas.findAll()
      .then((data) => {
        return new WsResponse(
          HttpStatus.OK,
          `Line count: ${data.length + 1}`,
          data
        );
      })
      .catch((err) => {
        throw new WsResponse(
          HttpStatus.INTERNAL_SERVER_ERROR,
          err.message,
          err
        );
      });
  },
  findAllIncludeParamModel: () => {
    return Canvas.findAll({
        include: [{
            model: Param,
            as: "PARAMS"
          },
          {
            model: Model,
            as: "MODEL"
          },
        ],
      })
      .then((data) => {
        return new WsResponse(
          HttpStatus.OK,
          `Line count: ${data.length + 1}`,
          data
        );
      })
      .catch((err) => {
        throw new WsResponse(
          HttpStatus.INTERNAL_SERVER_ERROR,
          err.message,
          err
        );
      });
  },

  findAllBelongsTo: (ID_MDM_MODEL) => {
    return Canvas.findAll({
        include: [{
          model: Param,
          as: "PARAMS",
          include: [{
            model: FormatDonnee,
            as: "FORMAT_DONNEE",
          }, ],
        }, ],
        where: {
          ID_MDM_MODEL: ID_MDM_MODEL
        }
      })
      .then((data) => {
        return new WsResponse(
          HttpStatus.OK,
          `Line count: ${data.length + 1}`,
          data
        );
      })
      .catch((err) => {
        throw new WsResponse(
          HttpStatus.INTERNAL_SERVER_ERROR,
          err.message,
          err
        );
      });
  },
  findAllActifBelongsTo: (ID_MDM_MODEL) => {
    return Canvas.findAll({
        include: [{
          model: Param,
          as: "PARAMS"
        }, ],
        where: {
          ID_MDM_MODEL: ID_MDM_MODEL,
          ACTIF: true
        }
      })
      .then((data) => {
        return new WsResponse(
          HttpStatus.OK,
          `Line count: ${data.length + 1}`,
          data
        );
      })
      .catch((err) => {
        throw new WsResponse(
          HttpStatus.INTERNAL_SERVER_ERROR,
          err.message,
          err
        );
      });
  },
  findOneIncludeParam: (ID_CANVAS) => {
    return Canvas.findOne({
        include: [{
          model: Param,
          as: "PARAMS"
            // ,include:[{
            //   model: FormatDonnee, as: "FORMAT_DONNEE"
            // }]
            ,
          where: {
            ID_CANVAS: ID_CANVAS
          }
        }, ],
      })
      .then((data) => {
        return new WsResponse(
          HttpStatus.OK,
          `Line count: ${data.length + 1}`,
          data
        );
      })
      .catch((err) => {
        throw new WsResponse(
          HttpStatus.INTERNAL_SERVER_ERROR,
          err.message,
          err
        );
      });
  },
  findHasLibelle: (libelle) => {
    return Canvas.findAll({
        where: {
          LIBELLE: libelle
        }
      })
      .then((data) => {
        return new WsResponse(
          HttpStatus.OK,
          `Line count: ${data.length + 1}`,
          data
        );
      })
      .catch((err) => {
        throw new WsResponse(
          HttpStatus.INTERNAL_SERVER_ERROR,
          err.message,
          err
        );
      });
  },
  HasPrimaryKey: (ID_CANVAS) => {
    return Canvas.findAll({
        include: [{
          model: Param,
          as: "PARAMS",
          where: {
            ID_CANVAS: ID_CANVAS,
            IS_PRIMARY_KEY: true
          }
        }, ],
      })
      .then((data) => {
        return new WsResponse(
          HttpStatus.OK,
          `Line count: ${data.length + 1}`,
          data
        );
      })
      .catch((err) => {
        throw new WsResponse(
          HttpStatus.INTERNAL_SERVER_ERROR,
          err.message,
          err
        );
      });
  },
  update: (CanvasObjet) => {
    return Canvas.update(CanvasObjet, {
        where: {
          ID_CANVAS: CanvasObjet.ID_CANVAS
        },
      })
      .then((data) => {
        return new WsResponse(HttpStatus.OK, "Opération succés", data);
      })
      .catch((err) => {
        throw new WsResponse(
          HttpStatus.INTERNAL_SERVER_ERROR,
          err.message,
          err
        );
      });
  },
  createWithParams: (CanvasObjet) => {
    return Canvas.create(CanvasObjet, {
        include: [{
          model: Param,
          as: "PARAMS",
        }, ],
      })
      .then((data) => {
        return new WsResponse(HttpStatus.OK, "Opération succés", data);
      })
      .catch((err) => {
        throw new WsResponse(
          HttpStatus.INTERNAL_SERVER_ERROR,
          err.message,
          err
        );
      });
  },
};