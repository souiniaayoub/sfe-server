const {
  Model,
  Param,
  db,
  Canvas
} = require("../../entities/gestionMDM");
const {
  WsResponse
} = require("../../helpers/ws-response/ws.response");
const {
  HttpStatus
} = require("../../helpers/http-status/http-status.enum");

module.exports = {
  create: async (ModelObjet) => {
    return Model.create(ModelObjet)
      .then((data) => {
        return new WsResponse(HttpStatus.OK, "Opération succés", data);
      })
      .catch((err) => {
        throw new WsResponse(
          HttpStatus.INTERNAL_SERVER_ERROR,
          err.message,
          err
        );
      });
  },
  findAll: () => {
    return Model.findAll()
      .then((data) => {
        return new WsResponse(
          HttpStatus.OK,
          `Line count: ${data.length + 1}`,
          data
        );
      })
      .catch((err) => {
        throw new WsResponse(
          HttpStatus.INTERNAL_SERVER_ERROR,
          err.message,
          err
        );
      });
  },
  findAllActif: () => {
    return Model.findAll({
        where: {
          ACTIF: true
        }
      })
      .then((data) => {
        return new WsResponse(
          HttpStatus.OK,
          `Line count: ${data.length + 1}`,
          data
        );
      })
      .catch((err) => {
        throw new WsResponse(
          HttpStatus.INTERNAL_SERVER_ERROR,
          err.message,
          err
        );
      });
  },
  findAllIncludeCanvas: () => {
    return Model.findAll({
        include: {
          model: Canvas,
          as: "CANVAS",
          // include: {
          //   model: Param,
          //   as: "PARAMS",
          // }
        },
      })
      .then((data) => {
        return new WsResponse(
          HttpStatus.OK,
          `Line count: ${data.length + 1}`,
          data
        );
      })
      .catch((err) => {
        throw new WsResponse(
          HttpStatus.INTERNAL_SERVER_ERROR,
          err.message,
          err
        );
      });
  },
  findHasLibelle: (libelle) => {
    return Model.findAll({
        where: {
          LIBELLE: libelle
        }
      })
      .then((data) => {
        return new WsResponse(
          HttpStatus.OK,
          `Line count: ${data.length + 1}`,
          data
        );
      })
      .catch((err) => {
        throw new WsResponse(
          HttpStatus.INTERNAL_SERVER_ERROR,
          err.message,
          err
        );
      });
  },
  // findAllIncludeParam: () => {
  //   // { include: Param }
  //   return Model.findAll({
  //     include: {
  //       model: Param,
  //       as: "PARAM",
  //     },
  //   })
  //     .then((data) => {
  //       return new WsResponse(
  //         HttpStatus.OK,
  //         `Line count: ${data.length + 1}`,
  //         data
  //       );
  //     })
  //     .catch((err) => {
  //       throw new WsResponse(
  //         HttpStatus.INTERNAL_SERVER_ERROR,
  //         err.message,
  //         err
  //       );
  //     });
  // },
  update: (ModelObjet) => {
    return Model.update(ModelObjet, {
        where: {
          ID_MDM_MODEL: ModelObjet.ID_MDM_MODEL
        },
      })
      .then((data) => {
        return new WsResponse(HttpStatus.OK, "Opération succés", data);
      })
      .catch((err) => {
        throw new WsResponse(
          HttpStatus.INTERNAL_SERVER_ERROR,
          err.message,
          err
        );
      });
  },
};