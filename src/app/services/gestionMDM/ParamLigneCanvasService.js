const {
  ParamLigneCanvas,
  db
} = require("../../entities/gestionMDM");
const {
  WsResponse
} = require("../../helpers/ws-response/ws.response");
const {
  HttpStatus
} = require("../../helpers/http-status/http-status.enum");

module.exports = {
  create: (ParamLigneCanvasObjet) => {
    return ParamLigneCanvas.create(ParamLigneCanvasObjet)
      .then((data) => {
        return new WsResponse(HttpStatus.OK, "Opération succés", data);
      })
      .catch((err) => {
        throw new WsResponse(
          HttpStatus.INTERNAL_SERVER_ERROR,
          err.message,
          err
        );
      });
  },
  update: (ParamLigneCanvasObjet) => {
    return ParamLigneCanvas.update(ParamLigneCanvasObjet, {
        where: {
          ID_PARAM_LIGNE_CANVAS: ParamLigneCanvasObjet.ID_PARAM_LIGNE_CANVAS
        }
      })
      .then((data) => {
        return new WsResponse(HttpStatus.OK, "Opération succés", data);
      })
      .catch((err) => {
        throw new WsResponse(
          HttpStatus.INTERNAL_SERVER_ERROR,
          err.message,
          err
        );
      });
  },
};