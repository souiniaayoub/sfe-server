const { Motif, db } = require("../../entities/gestionMDM");
const { WsResponse } = require("../../helpers/ws-response/ws.response");
const { HttpStatus } = require("../../helpers/http-status/http-status.enum");

module.exports = {
  create: (MotifObjet) => {
    return Motif.create(MotifObjet)
      .then((data) => {
        return new WsResponse(HttpStatus.OK, "Opération succés", data);
      })
      .catch((err) => {
        throw new WsResponse(
          HttpStatus.INTERNAL_SERVER_ERROR,
          err.message,
          err
        );
      });
  },
};
