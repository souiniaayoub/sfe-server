const {
  Canvas,
  Param,
  db
} = require("../../entities/gestionMDM");
const {
  WsResponse
} = require("../../helpers/ws-response/ws.response");
const {
  HttpStatus
} = require("../../helpers/http-status/http-status.enum");

module.exports = {
  create: (ParamObjet) => {
    return Param.create(ParamObjet)
      .then((data) => {
        return new WsResponse(HttpStatus.OK, "Opération succés", data);
      })
      .catch((err) => {
        throw new WsResponse(
          HttpStatus.INTERNAL_SERVER_ERROR,
          err.message,
          err
        );
      });
  },
  findAll: () => {
    return Param.findAll()
      .then((data) => {
        return new WsResponse(
          HttpStatus.OK,
          `Line count: ${data.length + 1}`,
          data
        );
      })
      .catch((err) => {
        throw new WsResponse(
          HttpStatus.INTERNAL_SERVER_ERROR,
          err.message,
          err
        );
      });
  },
  findAllIncludeCanvas: () => {
    return Param.findAll({
        include: [{
          model: Canvas,
          as: 'CANVAS'
        }]
      })
      .then((data) => {
        return new WsResponse(
          HttpStatus.OK,
          `Line count: ${data.length + 1}`,
          data
        );
      })
      .catch((err) => {
        throw new WsResponse(
          HttpStatus.INTERNAL_SERVER_ERROR,
          err.message,
          err
        );
      });
  },
  update: (ParamObjet) => {
    return Param.update(ParamObjet, {
        where: {
          ID_PARAM: ParamObjet.ID_PARAM
        },
      })
      .then((data) => {
        return new WsResponse(HttpStatus.OK, "Opération succés", data);
      })
      .catch((err) => {
        throw new WsResponse(
          HttpStatus.INTERNAL_SERVER_ERROR,
          err.message,
          err
        );
      });
  },
  findQueryDonneeData: (ID_PARAM) => {
    return Param.findOne({
        where: {
          ID_PARAM: ID_PARAM
        }
      })
      .then((data) => {
        return db.query(data.QUERY_DONNE_BASE, {
            type: db.QueryTypes.SELECT
          }).then((data) => {
            return new WsResponse(
              HttpStatus.OK,
              `Line count: ${data.length + 1}`,
              data
            );
          })
          .catch((err) => {
            throw new WsResponse(
              HttpStatus.INTERNAL_SERVER_ERROR,
              err.message,
              err
            );
          });
        // return new WsResponse(
        //   HttpStatus.OK,
        //   `Line count: ${data.length + 1}`,
        //   data
        // );
      })
      .catch((err) => {
        throw new WsResponse(
          HttpStatus.INTERNAL_SERVER_ERROR,
          err.message,
          err
        );
      });
  }
};