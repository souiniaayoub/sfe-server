const router = require("express").Router();

router.use("/model", require("./ModelController"));
router.use("/motif", require("./MotifController"));
router.use("/canvas", require("./CanvasContoller"));
router.use("/canvas-ligne", require("./CanvasLigneController"));
router.use("/param-ligne-canvas", require("./ParamLigneCanvasController"));
router.use("/param", require("./ParamController"));
router.use("/format-donnee", require("./FormatDonneeController"));

module.exports = router;