const router = require("express").Router();
const ParamService = require("../../services/gestionMDM/ParamService");
router.get("/findAll", async (req, res) => {
  ParamService.findAll()
    .then((response) => {
      res.status(response.statusCode).json(response);
    })
    .catch((error) => {
      res.status(error.statusCode).json(error);
    });
});
router.get("/findAllIncludeCanvas", async (req, res) => {
  ParamService.findAllIncludeCanvas()
    .then((response) => {
      res.status(response.statusCode).json(response);
    })
    .catch((error) => {
      res.status(error.statusCode).json(error);
    });
});

router.get("/findQueryDonneeData/:ID_PARAM", async (req, res) => {
  // ParamService.findAllIncludeCanvas()
  ParamService.findQueryDonneeData(req.params.ID_PARAM)
    .then((response) => {
      res.status(response.statusCode).json(response);
    })
    .catch((error) => {
      res.status(error.statusCode).json(error);
    });
});
router.post("/create", async (req, res) => {
  ParamService.create(req.body)
    .then((response) => {
      res.status(response.statusCode).json(response);
    })
    .catch((error) => {
      res.status(error.statusCode).json(error);
    });
});
router.put("/update", async (req, res) => {
  ParamService.update(req.body)
    .then((response) => {
      res.status(response.statusCode).json(response);
    })
    .catch((error) => {
      res.status(error.statusCode).json(error);
    });
});
module.exports = router;