const router = require("express").Router();
const ParamLigneCanvasService = require("../../services/gestionMDM/ParamLigneCanvasService");



router.post("/create", async (req, res) => {
    ParamLigneCanvasService.create(req.body)
        .then((response) => {
            res.status(response.statusCode).json(response);
        })
        .catch((error) => {
            res.status(error.statusCode).json(error);
        });
});

router.put("/update", async (req, res) => {
    ParamLigneCanvasService.update(req.body)
        .then((response) => {
            res.status(response.statusCode).json(response);
        })
        .catch((error) => {
            res.status(error.statusCode).json(error);
        });
});
module.exports = router;