const router = require("express").Router();
const CanvasLigneService = require("../../services/gestionMDM/CanvasLigneService");

router.get("/findAllIncludeParamLigneCanvas/:ID_CANVAS", async (req, res) => {
    CanvasLigneService.findAllIncludeParamLigneCanvas(req.params.ID_CANVAS)
        .then((response) => {
            res.status(response.statusCode).json(response);
        })
        .catch((error) => {
            res.status(error.statusCode).json(error);
        });
});
router.get("/NonValidParams/:ID_CANVAS_LIGNE", async (req, res) => {
    CanvasLigneService.NonValidParams(req.params.ID_CANVAS_LIGNE)
        .then((response) => {
            res.status(response.statusCode).json(response);
        })
        .catch((error) => {
            res.status(error.statusCode).json(error);
        });
});

router.get("/findAllByParamPK/:Value", async (req, res) => {
    CanvasLigneService.findAllByParamPK(req.params.Value)
        .then((response) => {
            res.status(response.statusCode).json(response);
        })
        .catch((error) => {
            res.status(error.statusCode).json(error);
        });
});

router.post("/createWithParams", async (req, res) => {
    CanvasLigneService.createWithParams(req.body)
        .then((response) => {
            res.status(response.statusCode).json(response);
        })
        .catch((error) => {
            res.status(error.statusCode).json(error);
        });
});
router.post("/create", async (req, res) => {
    CanvasLigneService.create(req.body)
        .then((response) => {
            res.status(response.statusCode).json(response);
        })
        .catch((error) => {
            res.status(error.statusCode).json(error);
        });
});
router.put("/update", async (req, res) => {
    CanvasLigneService.update(req.body)
        .then((response) => {
            res.status(response.statusCode).json(response);
        })
        .catch((error) => {
            res.status(error.statusCode).json(error);
        });
});
module.exports = router;