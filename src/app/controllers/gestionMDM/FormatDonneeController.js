const FormatDonneeService = require("../../services/gestionMDM/FormatDonneeService");

const router = require("express").Router();

router.get("/findAll", function (req, res) {
  FormatDonneeService.findAll()
    .then((response) => {
      res.status(response.statusCode).json(response);
    })
    .catch((error) => {
      res.status(error.statusCode).json(error);
    });
});
module.exports = router;
