const router = require("express").Router();
const CanvasService = require("../../services/gestionMDM/CanvasService");

router.get("/findAll", async (req, res) => {
  CanvasService.findAll()
    .then((response) => {
      res.status(response.statusCode).json(response);
    })
    .catch((error) => {
      res.status(error.statusCode).json(error);
    });
});
router.get("/findAllIncludeParamModel", async (req, res) => {
  CanvasService.findAllIncludeParamModel()
    .then((response) => {
      res.status(response.statusCode).json(response);
    })
    .catch((error) => {
      res.status(error.statusCode).json(error);
    });
});
router.get("/findHasLibelle/:Libelle", async (req, res) => {
  CanvasService.findHasLibelle(req.params.Libelle)
    .then((response) => {
      res.status(response.statusCode).json(response);
    })
    .catch((error) => {
      res.status(error.statusCode).json(error);
    });
});
router.get("/findOneIncludeParam/:ID_CANVAS", async (req, res) => {
  CanvasService.findOneIncludeParam(req.params.ID_CANVAS)
    .then((response) => {
      res.status(response.statusCode).json(response);
    })
    .catch((error) => {
      res.status(error.statusCode).json(error);
    });
});
router.get("/HasPrimaryKey/:ID_CANVAS", async (req, res) => {
  CanvasService.HasPrimaryKey(req.params.ID_CANVAS)
    .then((response) => {
      res.status(response.statusCode).json(response);
    })
    .catch((error) => {
      res.status(error.statusCode).json(error);
    });
});
router.get("/findAllBelongsTo/:ID_MDM_MODEL", async (req, res) => {
  CanvasService.findAllBelongsTo(req.params.ID_MDM_MODEL)
    .then((response) => {
      res.status(response.statusCode).json(response);
    })
    .catch((error) => {
      res.status(error.statusCode).json(error);
    });
});
router.get("/findAllActifBelongsTo/:ID_MDM_MODEL", async (req, res) => {
  CanvasService.findAllActifBelongsTo(req.params.ID_MDM_MODEL)
    .then((response) => {
      res.status(response.statusCode).json(response);
    })
    .catch((error) => {
      res.status(error.statusCode).json(error);
    });
});
router.post("/create", async (req, res) => {
  CanvasService.create(req.body)
    .then((response) => {
      res.status(response.statusCode).json(response);
    })
    .catch((error) => {
      res.status(error.statusCode).json(error);
    });
});
router.post("/createWithParams", async (req, res) => {
  CanvasService.createWithParams(req.body)
    .then((response) => {
      res.status(response.statusCode).json(response);
    })
    .catch((error) => {
      res.status(error.statusCode).json(error);
    });
});

router.put("/update", async (req, res) => {
  CanvasService.update(req.body)
    .then((response) => {
      res.status(response.statusCode).json(response);
    })
    .catch((error) => {
      res.status(error.statusCode).json(error);
    });
});
module.exports = router;