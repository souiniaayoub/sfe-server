const router = require("express").Router();
const MotifService = require("../../services/gestionMDM/MotifService");

router.post("/create", async (req, res) => {
    MotifService.create(req.body)
        .then((response) => {
            res.status(response.statusCode).json(response);
        })
        .catch((error) => {
            res.status(error.statusCode).json(error);
        });
});
module.exports = router;