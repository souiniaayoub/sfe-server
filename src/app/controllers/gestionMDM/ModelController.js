const router = require("express").Router();
const ModelService = require("../../services/gestionMDM/ModelService");

router.get("/findAll", async (req, res) => {
  ModelService.findAll()
    .then((response) => {
      res.status(response.statusCode).json(response);
    })
    .catch((error) => {
      res.status(error.statusCode).json(error);
    });
});
router.get("/findAllActif", async (req, res) => {
  ModelService.findAllActif()
    .then((response) => {
      res.status(response.statusCode).json(response);
    })
    .catch((error) => {
      res.status(error.statusCode).json(error);
    });
});
router.get("/findHasLibelle/:Libelle", async (req, res) => {
  ModelService.findHasLibelle(req.params.Libelle)
    .then((response) => {
      res.status(response.statusCode).json(response);
    })
    .catch((error) => {
      res.status(error.statusCode).json(error);
    });
});
router.get("/findAllIncludeCanvas", async (req, res) => {
  ModelService.findAllIncludeCanvas()
    .then((response) => {
      res.status(response.statusCode).json(response);
    })
    .catch((error) => {
      res.status(error.statusCode).json(error);
    });
});
router.post("/create", async (req, res) => {
  ModelService.create(req.body)
    .then((response) => {
      res.status(response.statusCode).json(response);
    })
    .catch((error) => {
      res.status(error.statusCode).json(error);
    });
});

router.put("/update", async (req, res) => {
  ModelService.update(req.body)
    .then((response) => {
      res.status(response.statusCode).json(response);
    })
    .catch((error) => {
      res.status(error.statusCode).json(error);
    });
});
module.exports = router;